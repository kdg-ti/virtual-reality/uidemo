﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonScript : MonoBehaviour
{
    [SerializeField]
    private GameObject cube;

    public void click() {
        cube.GetComponent<Rotator>().toggleRotating();
    }
}
