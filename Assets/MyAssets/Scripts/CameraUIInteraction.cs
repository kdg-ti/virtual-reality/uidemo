﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraUIInteraction : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField]
    private int uiDistance = 10;

    private GameObject lookAtButton;

    void Start() {

    }

    // Update is called once per frame
    void Update() {
        RaycastHit hit;
        Debug.Log("Checking if I hit a UI...");
        int layerMask = 1 << 5; //UI Mask
        if (Physics.Raycast(transform.position, transform.forward, out hit, uiDistance, layerMask)) {
            Debug.Log("UI object hit!");
            lookAtButton = hit.transform.gameObject;//.GetComponent<ButtonScript>().click();
        } else {
            lookAtButton = null;
        }

        if (lookAtButton != null && (Google.XR.Cardboard.Api.IsTriggerPressed || Input.GetKey(KeyCode.Space))) {
            Debug.Log("Clicking it...");
            lookAtButton.GetComponent<ButtonScript>().click();
        }
    }
}
