﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotator : MonoBehaviour
{
    // Start is called before the first frame update
    private int rotationSpeed = 360;
    private bool rotate = false;

    // Update is called once per frame
    void Update()
    {
        if (rotate) {
            transform.Rotate(Vector3.up, rotationSpeed * Time.deltaTime);
        }
    }

    public void toggleRotating() {
        rotate = !rotate;
    }
}
